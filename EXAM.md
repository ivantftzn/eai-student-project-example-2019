# CSCM602023 Advanced Programming (KKI) - 2020 - Final Programming Exam

> TODO: Replace this line with your full name and student ID (NPM)

TODO: Write the answers related to the programming exam here.

## 12 Factors

1. Codebase: this gitlab repository acts as the codebase of this application.
2. Dependencies: dependencies are listed in build.gradle
3. Config: 
4. Backing services:
5. Build, release, run:
6. Processes:
7. Port binding:
8. Concurrency:
9. Disposability:
10. Dev/prod Parity:
11. Logs:
12. Admin processes: